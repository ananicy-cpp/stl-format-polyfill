#ifndef STD_POLYFILL_HPP
#define STD_POLYFILL_HPP

/** Polyfills std::format from fmtlib (this is fine because the author of both
 * std::format and fmtlib are the same person) **/
#include <fmt/args.h>
#include <fmt/chrono.h>
#include <fmt/color.h>
#include <fmt/compile.h>
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/os.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>
#include <fmt/xchar.h>

// This is the kind of things you usually not do
namespace std {

/**
 * Selectively import declarations from fmtlib,
 * in order to only have declarations from std::format
 */

using ::fmt::format;
using ::fmt::format_to;
using ::fmt::format_to_n;
using ::fmt::formatted_size;

using ::fmt::vformat;
using ::fmt::vformat_to;

using ::fmt::basic_format_arg;
using ::fmt::formatter;

using ::fmt::basic_format_parse_context;
using ::fmt::format_parse_context;
using ::fmt::wformat_parse_context;

using ::fmt::basic_format_context;
using ::fmt::format_context;
using ::fmt::wformat_context;

using ::fmt::make_format_args;
using ::fmt::make_wformat_args;
using ::fmt::visit_format_arg;

using ::fmt::basic_format_args;
using ::fmt::format_args;
using ::fmt::wformat_args;

using ::fmt::format_error;
} // namespace std

#endif