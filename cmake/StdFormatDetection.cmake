include(CheckCXXSymbolExists)
message(STATUS "stdlib checks")
# set(PARENT_CONTEXT "${CMAKE_MESSAGE_CONTEXT}/")
set(CMAKE_MESSAGE_CONTEXT "${PARENT_CONTEXT}std")

check_cxx_symbol_exists(std::format "format" HAVE_STD_FORMAT)
if(NOT HAVE_STD_FORMAT)
    message(STATUS "std::format not available, using fmtlib as a replacement")
endif ()

option(STL_FORMAT_USE_EXTERNAL_FMTLIB "Use system fmtlib" ON)

set(CMAKE_MESSAGE_CONTEXT "${PARENT_CONTEXT}")

# fmt
if (NOT HAVE_STD_FORMAT)
    message(STATUS "Configuring fmtlib")
    set(CMAKE_MESSAGE_CONTEXT "${PARENT_CONTEXT}fmtlib")
    if (STL_FORMAT_USE_EXTERNAL_FMTLIB)
        find_package(fmt 8.0 QUIET)
    endif ()

    # Required to have exported fmt libraries
    set(FMT_MASTER_PROJECT ON CACHE BOOL "" FORCE)

    set(FMT_TEST ${ENABLE_TESTING} CACHE BOOL "" FORCE)

    if (NOT fmt_FOUND)
        include(FetchContent)
        FetchContent_Declare(fmtlib
                GIT_REPOSITORY https://github.com/fmtlib/fmt
                GIT_TAG 8.0.1)

        FetchContent_MakeAvailable(fmtlib)
    endif ()
    set(CMAKE_MESSAGE_CONTEXT "${PARENT_CONTEXT}")
endif()


# export(TARGETS stl_format_polyfill NAMESPACE stl_polyfills:: FILE ${PROJECT_BINARY_DIR}/stl_polyfills_targets.cmake)
